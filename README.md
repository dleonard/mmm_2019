# MMM_2019

You can clone this repository by launching the terminal (Press Ctrl + Alt + T) and typing:

git clone https://gitlab.ethz.ch/danielep/mmm_2019

The command will create a clone of 'mmm_2019' in your current working directory. You can update the local repository to match the content of the remote repository with the weekly updated material using the command:

git pull

Please, have a look at the student branch to find information on how to hand in your solution to the exercies. 
